package com.rapid.springjson.entity;

import com.rapid.springjson.model.AddressModel;
import com.rapid.springjson.model.CustomerModel;
import com.rapid.springjson.model.SchoolModel;
import lombok.*;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "customer_tab")
public class CustomerEntity {
    @Id
    @TableGenerator(name = "customer_id_generator", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue="customer_id", initialValue=0, allocationSize=0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "customer_id_generator")
    private Long id;

    @Column(name = "customer_fullName", length = 100, nullable = false)
    private String fullName;

    @Column(name = "customer_gender", length = 20, nullable = false)
    private String gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "customer_dateOfBirth")
    private Date dateOfBirth;

    @Column(name = "customer_placeOfBirth", length = 100, nullable = false)
    private String placeOfBirth;

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<AddressEntity> address = new ArrayList<>();

    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<SchoolEntity> schools = new ArrayList<>();

    public CustomerEntity(CustomerModel model) {
        BeanUtils.copyProperties(model,this);
    }

    public void addAddress(AddressEntity addressEntity){
        this.address.add(addressEntity);
        addressEntity.setCustomer(this);
    }

    public void addSchool(SchoolEntity schoolEntity){
        this.schools.add(schoolEntity);
        schoolEntity.setCustomer(this);
    }

    public void addListAddress(List<AddressModel> details){
        for(AddressModel item: details){
            this.addAddress(new AddressEntity(item));
        }
    }

    public void addListSchool(List<SchoolModel> details){
        for(SchoolModel item: details){
            this.addSchool(new SchoolEntity(item));
        }
    }

}
