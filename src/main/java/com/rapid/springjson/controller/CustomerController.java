package com.rapid.springjson.controller;

import com.rapid.springjson.model.CustomerModel;
import com.rapid.springjson.model.CustomerRequest;
import com.rapid.springjson.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Object> get(){
        List<CustomerModel> result = service.getAll();
        return ResponseEntity.ok().body(
                result
        );
    }

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody CustomerRequest request){
        return ResponseEntity.ok().body(
                service.saveAll(request)
        );
    }

//    @PostMapping
//    public ResponseEntity<Object> save(@RequestBody CustomerRequestModel request) {
//        return ResponseEntity.ok().body(request);
//    }

}
