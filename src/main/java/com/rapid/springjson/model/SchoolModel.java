package com.rapid.springjson.model;

import com.rapid.springjson.entity.SchoolEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SchoolModel {
    private Long id;
    private String title;
    private String name;
    private String level;

    public SchoolModel(SchoolEntity entity) {
        BeanUtils.copyProperties(entity, this);
    }
}
