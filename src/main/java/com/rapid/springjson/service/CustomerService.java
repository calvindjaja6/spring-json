package com.rapid.springjson.service;

import com.rapid.springjson.model.CustomerModel;
import com.rapid.springjson.model.CustomerRequest;
import com.rapid.springjson.model.CustomerResponse;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<CustomerModel> getAll();
    Optional<CustomerModel> getById(Long id);
    CustomerResponse saveAll(CustomerRequest request);
    Optional<CustomerModel> save(CustomerModel model);
    Optional<CustomerModel> update(Long id, CustomerModel model);
    Optional<CustomerModel> delete(Long id);

}
